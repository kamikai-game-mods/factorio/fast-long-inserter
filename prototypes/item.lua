data:extend({
  {
    type = "item",
    name = "fast-long-handed-inserter",
    icon = "__FastLongInserter__/graphics/icons/fast-long-handed-inserter.png",
    flags = {"goes-to-quickbar"},
    subgroup = "inserter",
    order = "c[fast-long-handed-inserter]",
    place_result = "fast-long-handed-inserter",
    stack_size = 50
  }
})