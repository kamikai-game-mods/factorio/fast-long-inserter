data:extend ({
  {
    type = "recipe",
    name = "fast-long-handed-inserter",
    enabled = false,
    ingredients =
    {
      {"electronic-circuit", 2},
      {"iron-plate", 1},
	  {"iron-gear-wheel",1},
      {"long-handed-inserter", 1}
    },
    result = "fast-long-handed-inserter",
    requester_paste_multiplier = 4
  }
})