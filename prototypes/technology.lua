data:extend({
{
      type = "technology",
      name = "fastlonginsertertech",
      icon = "__base__/graphics/technology/automation.png",
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "fast-long-handed-inserter"
        }
      },
      prerequisites = {"automation","logistics"},
      unit =
      {
        count = 10,
        ingredients =
        {
          {"science-pack-1", 2},
        },
        time = 20
      }
  }
})